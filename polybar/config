; ~/.config/polybar/config
; by Richard

[colors]
background = #111
foreground = #fff
primary    = #80f
secondary  = #f00

[bar/example]
monitor             = VGA-0
bottom              = false
width               = 100%
height              = 20
fixed-center        = true
background          = ${colors.background}
foreground          = ${colors.foreground}
line-size           = 2
border-color        = #000
module-margin-right = 1
font-0              = "xos4 Terminus:pixelsize=8:antialias=false;1"
font-1              = "Font Awesome 5 Free:style=Regular:pixelsize=8;1"
font-2              = "Font Awesome 5 Free:style=Solid:pixelsize=8;1"
modules-left        = i3
modules-center      = date
modules-right       = moc fs_root fs_temp fs_home fs_reserve cpu memory lan language volume
tray-position       = right
tray-maxsize        = 16
scroll-up           = i3wm-wsnext
scroll-down         = i3wm-wsprev
cursor-click        = pointer
padding-right       = 1

[module/moc]
interval      = 1
type          = custom/script
exec          = ~/.config/polybar/moc.sh
label         = %output:0:64:...%
format-prefix = " "
format        = ${colors.primary}

[module/language]
type             = internal/xkeyboard
blacklist-0      = num lock
blacklist-1      = caps lock
blacklist-2      = scroll lock
format-prefix    = " "
label-layout     = %layout%
;format-underline = ${colors.primary}

[module/fs]
type                       = internal/fs
interval                   = 60
format-mounted-prefix      = " "
format-umounted-prefix     = " "
;format-mounted-underline   = ${colors.primary}
format-unmounted-underline = ${colors.secondary}

[module/fs_root]
inherit       = module/fs
mount-0       = /
label-mounted = ROOT %percentage_used:2%%

[module/fs_temp]
inherit         = module/fs
mount-0         = /tmp
label-mounted   = TEMP %percentage_used:2%%
label-unmounted = TEMP

[module/fs_home]
inherit         = module/fs
mount-0         = /home
label-mounted   = HOME %percentage_used:2%%
label-unmounted = HOME

[module/fs_reserve]
inherit         = module/fs
mount-0         = /media/reserve
label-mounted   = RESERVE %percentage_used:2%%
label-unmounted = RESERVE

[module/i3]
type                    = internal/i3
format                  = <label-state> <label-mode>
index-sort              = true
wrapping-scroll         = true
label                   = %index%
label-foreground        = ${colors.foreground}
label-mode-background   = ${colors.primary}
label-focused-underline = ${colors.primary}
label-urgent-underline  = ${colors.secondary}

[module/cpu]
type             = internal/cpu
interval         = 1
label            =  %percentage:2%%
;format-underline = ${colors.primary}

[module/memory]
type             = internal/memory
interval         = 1
label            =  %percentage_used:2%%
;format-underline = ${colors.primary}

[module/lan]
type                          = internal/network
interface                     = enp2s0
interval                      = 5
format-connected              = 
format-connected-underline    = ${colors.primary}
format-disconnected           = 
format-disconnected-underline = ${colors.secondary}

[module/date]
type             = internal/date
interval         = 1
;format-underline = ${colors.primary}
format-prefix    = %{T1} %{T-}
label            = %date% %time%
date             = "%a, %d %b"
date-alt         = "%A, %d %B %Y"
time             = %H:%M
time-alt         = %H:%M:%S

[module/volume]
type                    = internal/volume
label-volume            = %percentage:3%%
label-muted             = MUTED
format-volume-underline = ${colors.primary}
format-muted-underline  = ${colors.secondary}
format-volume-prefix    = %{T2} %{T-}
format-muted-prefix     = %{T2} %{T-}

[settings]
throttle-output     = 1
screenchange-reload = true
