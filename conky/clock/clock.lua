-- ~/.config/conky/clock.lua
-- by Richard

require 'cairo'

function conky_clock()
    -- init cairo
    if conky_window == nil then return end
    local cs = cairo_xlib_surface_create(
        conky_window.display,
        conky_window.drawable,
        conky_window.visual,
        conky_window.width,
        conky_window.height)

    -- create objects
    obj_bg = cairo_create(cs)
    obj_hour = cairo_create(cs)
    obj_minute = cairo_create(cs)

    -- create texture
    local bg = cairo_image_surface_create_from_png(
        "/home/richard/.config/conky/clock/background.png")
    local minute = cairo_image_surface_create_from_png(
        "/home/richard/.config/conky/clock/minute_arrow.png")
    local hour = cairo_image_surface_create_from_png(
        "/home/richard/.config/conky/clock/hour_arrow.png")

    -- set
    cairo_translate(obj_minute, 512 * 0.5, 512 * 0.5)
    cairo_translate(obj_hour, 512 * 0.5, 512 * 0.5)
    cairo_rotate(obj_minute, math.rad(os.date("%M") * 6))
    cairo_rotate(obj_hour, math.rad(os.date("%H") * 30))
    cairo_set_source_surface(obj_bg, bg, 0, 0)
    cairo_set_source_surface(obj_minute, minute, -512 * 0.5, -512 * 0.5)
    cairo_set_source_surface(obj_hour, hour, -512 * 0.5, -512 * 0.5)

    -- draw
    cairo_paint(obj_bg)
    cairo_paint(obj_minute)
    cairo_paint(obj_hour)

    -- destroy
    cairo_surface_destroy(bg)
    cairo_surface_destroy(minute)
    cairo_surface_destroy(hour)
    cairo_destroy(obj_bg)
    cairo_destroy(obj_minute)
    cairo_destroy(obj_hour)
    cairo_surface_destroy(cs)
    cr = nil
end
